# Bash may be a lost cause for getting the config into XDG_CONFIG_HOME.
# If there is a way to load additional configs, this should at least be
# loaded from XDG_CONFIG_HOME.

alias l="ls -l"
alias ll="ls -l"
alias la="ls -la"
alias ldir="ls -al | grep ^d"
alias del="gvfs-trash"
alias rmr="rm -rf"
alias dir="ls"
alias vim="nvim"
alias vvim="vim"
# Do this by symlinking /usr/games/steam to /usr/bin/firejail instead
#alias steam="firejail --private=/home/neon/jails/steam ./steam"
