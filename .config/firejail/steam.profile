# This is not prepared yet
#include ${XDG_CONFIG_DIR}/firejail/common.inc

## From disable-common.inc
read-only ${HOME}/.Xauthority
read-only ${HOME}/.config/dconf

## From whitelist-var-common.inc
whitelist /var/lib/dbus
whitelist /var/lib/menu-xdg
whitelist /var/cache/fontconfig
whitelist /var/tmp
whitelist /var/run
whitelist /var/lock

# my most important jail requirements
whitelist ${HOME}/Software/
private ${HOME}/Documents/jails/steam/
noblacklist /usr/bin

# needed for STEAM_RUNTIME_PREFER_HOST_LIBRARIES=1 to work
noblacklist /sbin
noblacklist /usr/sbin

nogroups
nonewprivs
noroot
notv
nou2f
protocol unix,inet,inet6,netlink
# seccomp cause sometimes issues (see #2951, #3267),
# comment it or add 'ignore seccomp' to steam.local if so.
seccomp !kcmp,!ptrace
shell none
# private-etc breaks a small selection of games on some systems, comment to support those
private-etc alternatives,alternatives,asound.conf,bumblebee,ca-certificates,crypto-policies,dbus-1,drirc,fonts,group,gtk-2.0,gtk-3.0,host.conf,hostname,hosts,ld.so.cache,ld.so.conf,ld.so.conf.d,ld.so.preload,localtime,lsb-release,machine-id,mime.types,nvidia,os-release,passwd,pki,pulse,resolv.conf,services,ssl
#private-tmp
