" Lines -- for dealing with how lines are drawn and typed.
" Sourced by $VIMINIT

" Start in insert for blank files
autocmd VimEnter * if BufferFileEmpty() | startinsert | endif

" Start in insert for new files (previous autocmd doesn't catch
" instances where 'split' is used)
autocmd BufNewFile * startinsert

" Start in insert for git commits
autocmd FileType gitcommit exec 'autocmd VimEnter * startinsert'

" Turn line count on. Setting both absolute and relative makes hybrid.
set number relativenumber

" Set literal tabs to appear 4 spaces wide (default is 8)
" BUG - This causes two tabs of width 4 to be entered together, unless
"   the line begins with a comment. This is VERY odd behavior.
"set tabstop=4

