" Init -- Simply sources other .vim files in the same folder for config
" 
" NOTE - Neovim depends on a provider (xclip or xsel in Linux) to
" manipulate the keyboard. Installing xclip is recommended.

" BUG - $XDG_CONFIG_HOME is empty when interpreted here because, I
"   assume, it can't evaluate $HOME inside of XDG_CONFIG_HOME, for some
"   reason. More strangely, it can evaluate $HOME on its own.
"source $XDG_CONFIG_HOME/vim/functions.vim

source $HOME/.config/vim/functions.vim
source $HOME/.config/vim/general.vim
source $HOME/.config/vim/rebindings.vim
source $HOME/.config/vim/lines.vim

