" Rebindings -- different keystrokes for different folks.
" Sourced from $VIMINIT

" Split navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

set wildchar=<Tab> " Tab is default in Vim

