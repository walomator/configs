" General
" Sourced by $VIMINIT

" Disable modeline: custom editor options set within the file being
" edited on. Because I don't like it and it can cause vulnerabilities.
set modelines=0
set nomodeline

" Keep improvements of Vim turned on, such as arrow key support
set nocompatible " This is always true in Nvim

" Make changes to the editor appropriate for the filetype opened. For
" some reason, setting 'filetype' off doesn't disable .rst expanding
" sections unless also setting 'syntax' off.
filetype on

" Fine-tune auto-completion.
set nowildmenu
set wildmode=longest,list,full

" Abbreviate nohlsearch/nohl even more.
" 'cnoreabbrev' is used instead of 'command' because of stern rules
" against making user-defined commands lowercase.
cnoreabbrev nh nohlsearch

" Update vimrc immediately on write. DOES NOT WORK!
" DEV - Get this working
"autocmd! BufWritePost $MYVIMRC source $MYVIMRC

