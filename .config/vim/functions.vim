" Functions -- for use in other parts of the Vim config.
" Sourced by $VIMINIT

" Returns true if the current file is empty.
" @% will get the current file. I'm not sure why.
function BufferFileEmpty()
	return (getfsize(@%) == 0)
"	return empty(expand("@%")) " This method doesn't work
endfunction

