# This config will just load up config files in conf.d/

# Sources are explicit for now. It may or may not be a big deal to
# source every file in the conf.d directory. The config files are
# numbered to specify the order in which they should be loaded.
source $ZDOTDIR/conf.d/00-init.zsh
source $ZDOTDIR/conf.d/01-variables.zsh
source $ZDOTDIR/conf.d/10-general.zsh
source $ZDOTDIR/conf.d/20-visual.zsh
source $ZDOTDIR/conf.d/90-final.zsh


