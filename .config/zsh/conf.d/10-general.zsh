# General


# Autocompletion:
# Enable
autoload -Uz compinit
compinit
# For autocompletion with an arrow-key driven interface
zstyle ':completion:*' menu select
# For autocompletion of command line switches for aliases
setopt COMPLETE_ALIASES


# Set the history line-size (I don't know the difference between these
# two options).
HISTSIZE=1000
SAVEHIST=1000


# Set Zsh to use Vim-like keybindings in some places
bindkey -v

