# Variables
# There isn't much use for this section of config. It is intended for
# general-purpose environment variables that one would only want while
# using Zsh.
# 
# Environment variables intended to modify Zsh itself could go here.
# They also go in /etc/zsh/zshenv if all users of Zsh should be
# affected, such as in the case of setting a custom starting config
# location. Otherwise they go in whatever config file fits the type of
# change that the env var is making, sometimes in General.

