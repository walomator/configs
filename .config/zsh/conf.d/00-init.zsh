# Initial configuration file loaded (of the files in this folder). Do
# anything here that should occur with highest priority.

# Use the more XDG base directory-friendly alternative to ~/.profile
source $XDG_CONFIG_HOME/profile

# user-dirs.dirs provides more environment variables, but do not seem
# to be set by default
source $XDG_CONFIG_HOME/user-dirs.dirs

# If shell-agnostic, interactive-only behavior is desired, it is
# written into $XDG_CONFIG_HOME/profile-interactive. The following test
# reads the value of the variable $-, which contains "i" if the shell
# was started in interactive mode.
# 
# DEV - The test may be unnecessary, as $ZDOTDIR may only be sourced in
#   interactive shells, if it is similar to Bash's .bashrc, but it is
#   probably a good extra precaution anyway; look into zsh's behavior.
# 
if [[ $- == *i* ]];
then
    source $XDG_CONFIG_HOME/profile-interactive
fi

# Move the location of the .zcompdump file out of $ZDOTDIR and into the
# cache. The directory file may need to be created first.
compinit -d ~/.cache/zsh/zcompdump-$ZSH_VERSION

